﻿# https://learn-powershell.net/2014/09/30/using-mutexes-to-write-data-to-the-same-logfile-across-processes-with-powershell/

[System.Threading.Mutex]$mutant;
try
{
    # Obtain a system mutex that prevents more than one deployment taking place at the same time.
    [bool]$wasCreated = $false;
    $mutant = New-Object System.Threading.Mutex($true, "Global\MyMutex", [ref] $wasCreated);        
    if (!$wasCreated)
    {            
        Write-Host "waiting on mutex"
        $mutant.WaitOne();
    }

    ### Do Work ###
    Start-Sleep 5
    Write-Host waiting
}
finally
{       
    $mutant.ReleaseMutex(); 
    $mutant.Dispose();
}