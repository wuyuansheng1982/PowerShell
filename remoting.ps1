Invoke-Command -ComputerName chinaapp1 {hostname}
Invoke-Command -UseSSL -Port 443 -ThrottleLimit 64 -{Get-Service} -ComputerName chinaapp1 -Credential (Get-Credential)

Install-WindowsFeature -Name WindowsPowerShellWebAccess
Get-Help *pswa*

Install-PswaWebApplication -UseTestCertificate
Add-PswaAuthorizationRule -userName <Domain\User | Computer\user> -ComputerName <Computer> -ConfigurationName AdminsOnly
