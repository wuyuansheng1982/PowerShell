param (
	$inputfile
)
$logData = Get-Content -Path $inputfile

$ipPattern = "\b(?:\d{1,3}\.){3}\d{1,3}\b"

$ipAddresses = $logData | Select-String -Pattern $ipPattern | ForEach-Object {
    $_.Matches.Value
}

$apiKey = "f75f63d25dea041de9928465421428e6"  # Replace with your actual API key
$topIPAddress = $ipAddresses | Group-Object | Sort-Object Count -Descending | Select-Object -First 5 -Skip 5
$topIPAddress | ForEach-Object {
    $IP = $_.Name
    $locationData = Invoke-RestMethod -Uri "http://api.ipapi.com//$($IP)?access_key=$apiKey"
    $locationData
}


