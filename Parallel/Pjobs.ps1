﻿$start = Get-Date

# get all hotfixes
$task1 = { Get-Hotfix }

# get all scripts in your profile
$task2 = { Get-Service | Where-Object Status -eq Running }

# parse log file
$task3 = { Get-Content -Path $env:windir\windowsupdate.log | Where-Object { $_ -like '*successfully installed*' } }

# run 2 tasks in the background, and 1 in the foreground task
$job1 =  Start-Job -ScriptBlock $task1 
$job2 =  Start-Job -ScriptBlock $task2 
$job3 =  Start-Job -ScriptBlock $task3

# wait for the remaining tasks to complete (if not done yet)
$null = Wait-Job -Job $job1, $job2, $job3

# now they are done, get the results
$result1 = Receive-Job -Job $job1
$result2 = Receive-Job -Job $job2
$result3 = Receive-Job -Job $job3

# discard the jobs
Remove-Job -Job $job1, $job2, $job3

$end = Get-Date
Write-Host -ForegroundColor Red ($end - $start).TotalSeconds