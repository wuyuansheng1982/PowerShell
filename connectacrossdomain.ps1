Param (
    [Parameter(Mandatory=$True)]
    [string]$user
)
if (-not (test-path ".\domain_$user.txt")) {
    $Secure = Read-Host -assecurestring "Please enter your password"
    $Encrypted = ConvertFrom-SecureString -SecureString $Secure -key (1..16)
    $Encrypted | Set-Content ".\domain_$user.txt"
}

$Password = get-content ".\domain_$user.txt" | ConvertTo-SecureString -Key (1..16)
$cred = New-Object System.Management.Automation.PSCredential(("domain\" + $user), $Password)
Enter-PSSession $server.$domain.com -Credential $cred

